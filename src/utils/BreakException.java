package utils;

public class BreakException extends RuntimeException{

	private static final long serialVersionUID = -2431473453374656968L;

	public BreakException(String arg) {
		super(arg);
	}
}

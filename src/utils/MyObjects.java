package utils;

public class MyObjects {
    public static <T> T nonNullOrDefault(T obj, T defaultVal) {
        if (obj != null)
            return obj;
        return defaultVal;
    }
}

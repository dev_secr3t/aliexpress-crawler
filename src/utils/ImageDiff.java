package utils;
	import java.awt.image.BufferedImage;
//import java.awt.image.DataBuffer;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import javax.imageio.ImageIO;
	 
	public class ImageDiff {
		
	    public static double getDifferencePercent(String filePath, BufferedImage img2) {
	    	BufferedImage img1 = null;
			try {
				img1 = ImageIO.read(new BufferedInputStream(Files.newInputStream(Paths.get(filePath))));
			} catch (IOException e) {
				return 100.00;
			}
	    	int width = img1.getWidth();
	    	double height = img1.getHeight();
	    	int width2 = img2.getWidth();
	    	double height2 = img2.getHeight();
	    	if (width != width2 || height != height2) {
//	    		throw new IllegalArgumentException(String.format("Images must have the same dimensions: (%d,%d) vs. (%d,%d)", width, height, width2, height2));
	    		return 100.00;
	    	}
	    	
	    	long diff = 0;
//	    	int start = (int)(height*0.5 - height*0.05);
	    	int start = 0;
	    	int end = (int) height;
	    	for (int y = start; y < end; y++) {
	    		for (int x = 0; x < width; x++) {
	    			diff += pixelDiff(img1.getRGB(x, y), img2.getRGB(x, y));
	    		}
	    	}
	    	long maxDiff = 3L * 255 * (int)(width * (end-start));
	    	
	    	
	    	return 100.0 * diff / maxDiff;
	    }
	 
/*	    public static double getFlipDifferencePercent (BufferedImage img1, BufferedImage img2) {
	    	int width = img1.getWidth();
	    	double height = img1.getHeight();
	    	int width2 = img2.getWidth();
	    	double height2 = img2.getHeight();
	    	if (width != width2 || height != height2) {
	    		throw new IllegalArgumentException(String.format("Images must have the same dimensions: (%d,%d) vs. (%d,%d)", width, height, width2, height2));
	    	}
	    	
	    	long diff = 0;
	    	for (int y = (int)(height*0.5 - height*0.01); y < height*0.5 + height*0.01; y++) {
	    		for (int x = 0; x < width; x++) {
	    			diff += pixelDiff(img1.getRGB(width-x - 1, y), img2.getRGB(x, y));
	    		}
	    	}
	    	long maxDiff = 3L * 255 * (int)(width * height*0.02);
	    	
	    	
	    	return 100.0 * diff / maxDiff;
	    }
	    */
	    private static int pixelDiff(int rgb1, int rgb2) {
	        int r1 = (rgb1 >> 16) & 0xff;
	        int g1 = (rgb1 >>  8) & 0xff;
	        int b1 =  rgb1        & 0xff;
	        int r2 = (rgb2 >> 16) & 0xff;
	        int g2 = (rgb2 >>  8) & 0xff;
	        int b2 =  rgb2        & 0xff;
	        return Math.abs(r1 - r2) + Math.abs(g1 - g2) + Math.abs(b1 - b2);
	    }

/*	    public static double getDifferencePercentWithDataBuffer(String filePath, BufferedImage img) {
	    	double percentage = 0;
	    	try {
	    		BufferedImage target = ImageIO.read(new BufferedInputStream(Files.newInputStream(Paths.get(filePath))));
	    		DataBuffer targetDB = target.getData().getDataBuffer();
	    		int targSize = targetDB.getSize();
	    		DataBuffer compareDB = img.getData().getDataBuffer();
	    		int compareSize = compareDB.getSize();
	    		int count = 0;
				int start = (int) (targSize * 0.5 - targSize * 0.01);
				int end = (int) (targSize * 0.5 + targSize * 0.01);
				if (targSize == compareSize) {
					for (int i = start; i < end; i++) {
						if (targetDB.getElem(i) == compareDB.getElem(i)) {
							count = count + 1;
						}
					}
					percentage = (count * 100) / (end - start);
				} else {
					percentage = 0;
				}
			} catch (Exception e) {
				System.out.println("Failed to compare image files ...");
			}
			System.gc();
			return percentage;
	    }*/

	    
	}

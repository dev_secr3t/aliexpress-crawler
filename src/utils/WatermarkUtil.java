package utils;

import java.awt.AlphaComposite;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;

public class WatermarkUtil {
	
	public static BufferedImage watermark(BufferedImage originalImage,
	        BufferedImage watermarkImage) throws IOException {

	    int imageWidth = originalImage.getWidth();
	    int imageHeight = originalImage.getHeight();

	    // We create a new image because we want to keep the originalImage
	    // object intact and not modify it.
	    BufferedImage bufferedImage = new BufferedImage(imageWidth,
	            imageHeight, BufferedImage.TYPE_INT_RGB);
	    Graphics2D g2d = (Graphics2D) bufferedImage.getGraphics();
	    g2d.drawImage(originalImage, 0, 0, null);
	    g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
	            RenderingHints.VALUE_ANTIALIAS_ON);

	    int x = (originalImage.getWidth() - watermarkImage.getWidth()) / 2;
	    int y = (originalImage.getHeight() - watermarkImage.getHeight()) / 2;
	    
	    g2d.drawImage(watermarkImage, x, y, null);

	    return bufferedImage;
	}
	
	public static void addImageWatermark(File watermarkImageFile, File sourceImageFile, File destImageFile) {
		try {
			BufferedImage sourceImage = ImageIO.read(sourceImageFile);
			BufferedImage watermarkImage = ImageIO.read(watermarkImageFile);

			// initializes necessary graphic properties
			Graphics2D g2d = (Graphics2D) sourceImage.getGraphics();
			AlphaComposite alphaChannel = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.8f);
			g2d.setComposite(alphaChannel);

			// calculates the coordinate where the image is painted
			int topLeftX = (sourceImage.getWidth() - watermarkImage.getWidth()) / 2;
			int topLeftY = (sourceImage.getHeight() - watermarkImage.getHeight()) / 2;

			// paints the image watermark
			g2d.drawImage(watermarkImage, topLeftX, topLeftY, null);

			ImageIO.write(sourceImage, "png", destImageFile);
			g2d.dispose();

			System.out.println("The image watermark is added to the image.");

		} catch (IOException ex) {
			System.err.println(ex);
		}
	}
	
	public static void addImageWatermark(File watermarkImageFile, BufferedImage sourceImage, File destImageFile) {
		if(sourceImage == null)
			return;
		try {
			BufferedImage watermarkImage = ImageIO.read(watermarkImageFile);

			// initializes necessary graphic properties
			Graphics2D g2d = (Graphics2D) sourceImage.getGraphics();
			AlphaComposite alphaChannel = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.7f);
			g2d.setComposite(alphaChannel);

			// calculates the coordinate where the image is painted
			int topLeftX = (sourceImage.getWidth() - watermarkImage.getWidth()) / 2;
			int topLeftY = (sourceImage.getHeight() - watermarkImage.getHeight()) / 2;

			// paints the image watermark
			g2d.drawImage(watermarkImage, topLeftX, topLeftY, null);
			ImageIO.write(sourceImage, "jpg", destImageFile);
			g2d.dispose();
		} catch(FileNotFoundException ex) {
			
		} catch (IOException ex) {
			System.err.println(ex);
		}
	}
	public static void addImageWatermark(File watermarkImageFile, BufferedImage sourceImage, String destImageFilePath) {
		File destImageFile = new File(destImageFilePath);
		if(destImageFile.exists()) {
			try {
				FileUtils.forceDelete(destImageFile);
				destImageFile = new File(destImageFilePath);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		try {
			BufferedImage watermarkImage = ImageIO.read(watermarkImageFile);
			
			// initializes necessary graphic properties
			Graphics2D g2d = (Graphics2D) sourceImage.getGraphics();
			AlphaComposite alphaChannel = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.6f);
			g2d.setComposite(alphaChannel);
			
			// calculates the coordinate where the image is painted
			int topLeftX = (sourceImage.getWidth() - watermarkImage.getWidth()) / 2;
			int topLeftY = (sourceImage.getHeight() - watermarkImage.getHeight()) / 2;
			
			// paints the image watermark
			g2d.drawImage(watermarkImage, topLeftX, topLeftY, null);
			ImageIO.write(sourceImage, "jpg", destImageFile);
			g2d.dispose();
			
		} catch (IOException ex) {
			System.err.println(ex);
		}
	}
}

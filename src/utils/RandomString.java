package utils;

import java.util.Random;

public class RandomString {

	public static String getRandomString(int n) {
		Random rnd = new Random();
		StringBuffer sb = new StringBuffer();
		for(int i = 0; i < n; i++) {
			if(rnd.nextBoolean()) {
				sb.append( (char)(rnd.nextInt(26)+97) );
			} else {
				sb.append(rnd.nextInt(10));
			}
		}
		return sb.toString();
	}
}

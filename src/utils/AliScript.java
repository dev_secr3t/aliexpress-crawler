package utils;

public class AliScript {
    public static String GET_PRICE_SCRIPT =
            "var priceString = runParams.data.priceModule.formatedActivityPrice || runParams.data.priceModule.formatedPrice;\n" +
                    "return priceString.match(/(\\d+\\.\\d+)?( - )?(\\d+\\.\\d+)/)[3]";

    public static String GET_PROPERTY_MAP = "var result = {};\n" +
            "var optionNames = '';\n" +
            "var optionValues = '';\n" +
            "var optionImgs = '';\n" +
            "let beginP = '<p align=\\'center\\'>';\n" +
            "let endP = '</p>';\n" +
            "let logo = '';\n" +
            "let beginSpan = '<span style=\\'font-size : 25px; color : brown\\'>';\n" +
            "let endSpan = '</span>'\n" +
            "let beginStrong = '<strong>';\n" +
            "let endStrong = '</strong>';\n" +
            "\n" +
            "\n" +
            "runParams\n" +
            ".data\n" +
            ".skuModule\n" +
            ".productSKUPropertyList\n" +
            ".forEach(e => {\n" +
            "   if(e.skuPropertyName !== 'Ships From') {\n" +
            "       optionNames += e.skuPropertyName + '\\r\\n';\n" +
            "       var num = 1;\n" +
            "       e.skuPropertyValues.forEach(value => {\n" +
            "           var optionStr = value.propertyValueDisplayName || value.propertyValueName;\n" +
            "           if(optionStr.length > 25) {\n" +
            "               optionStr = optionStr.substr(0, 25);\n" +
            "           }\n" +
            "           var valueString =\n" +
            "               num + \n" +
            "               ' - ' + \n" +
            "               optionStr + \n" +
            "               ',' ;\n" +
            "            optionValues += valueString;\n" +
            "            var imgUrl = value.skuPropertyImagePath || '';\n" +
            "            if(imgUrl !== '') {\n" +
            "                if(imgUrl.startsWith('://')) \n" +
            "                   imgUrl += 'https'; \n" +
            "                var img = document.createElement('IMG');\n" +
            "                img.src = imgUrl;\n" +
            "                optionImgs +=\n" +
            "                    beginP +\n" +
            "                    logo +\n" +
            "                    beginSpan +\n" +
            "                    beginStrong +\n" +
            "                    num +\n" +
            "                    endStrong +\n" +
            "                    ' - ' + optionStr + \n" +
            "                    endSpan +\n" +
            "                    endP +\n" +
            "                    beginP +\n" +
            "                    img.outerHTML +\n" +
            "                    endP +\n" +
            "                    \"<br/><br/>\"\n" +
            "            }\n" +
            "            num++;\n" +
            "       });\n" +
            "       optionValues = optionValues.replace(/,$/, '');\n" +
            "       optionValues += '\\r\\n';\n" +
            "}\n" +
            "});\n" +
            "\n" +
            "result.optionNames = optionNames;\n" +
            "result.optionValues = optionValues;\n" +
            "result.optionImgs = optionImgs;\n" +
            "\n" +
            "return result;";

    public static String GET_DESCRIPTION_URL = "return runParams.data.descriptionModule.descriptionUrl";
}

package utils;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import javax.imageio.ImageIO;

public class ImageResize {
//	private final static double RESIZE_VALUE = 640.0;
	public static BufferedImage resize(String url, double size) {
		try {
		Image img = ImageIO.read(new URL(url));
			double imgH = img.getHeight(null);
			double imgW = img.getWidth(null);
			int changeH;
			int changeW;
			if(imgH > imgW) {	// 세로가 더 긴 비율을 가질 때
				double rat = size / imgH;
				changeH = (int) (imgH * rat);
				changeW = (int) (imgW * rat);
			} else if(imgH < imgW) { //가로가 더 긴 비율을 가질 때
				double rat = size / imgW;
				changeH = (int) (imgH * rat);
				changeW = (int) (imgW * rat);
			} else {	// 정사각형일 때
				changeH = (int) size;
				changeW = (int) size;
			}
			
            // 이미지 리사이즈
            // Image.SCALE_DEFAULT : 기본 이미지 스케일링 알고리즘 사용
            // Image.SCALE_FAST    : 이미지 부드러움보다 속도 우선
            // Image.SCALE_REPLICATE : ReplicateScaleFilter 클래스로 구체화 된 이미지 크기 조절 알고리즘
            // Image.SCALE_SMOOTH  : 속도보다 이미지 부드러움을 우선
            // Image.SCALE_AREA_AVERAGING  : 평균 알고리즘 사용	
			
			
			BufferedImage bi = new BufferedImage(changeW, changeH, BufferedImage.TYPE_INT_RGB);
	        Graphics g = bi.getGraphics();
	        g.drawImage(img.getScaledInstance(changeW, changeH, Image.SCALE_AREA_AVERAGING), 0, 0, null);
	        g.dispose();
			return bi;
		} catch (MalformedURLException e) {
			return null;
		} catch (IOException e) {
			return null;
		} catch (NullPointerException e) {
			return null;
		} catch (IllegalArgumentException e) {
			return null;
		} catch (Exception e) {
			return null;
		}
	}
	
	public static BufferedImage resizeWithFile(String filePath, double size) {
		try {
			Image img = ImageIO.read(new File(filePath));
			double imgH = img.getHeight(null);
			double imgW = img.getWidth(null);
			int changeH;
			int changeW;
			if(imgH > imgW) {	// 세로가 더 긴 비율을 가질 때
				double rat = size / imgH;
				changeH = (int) (imgH * rat);
				changeW = (int) (imgW * rat);
			} else if(imgH < imgW) { //가로가 더 긴 비율을 가질 때
				double rat = size / imgW;
				changeH = (int) (imgH * rat);
				changeW = (int) (imgW * rat);
			} else {	// 정사각형일 때
				changeH = (int) size;
				changeW = (int) size;
			}
			
			// 이미지 리사이즈
			// Image.SCALE_DEFAULT : 기본 이미지 스케일링 알고리즘 사용
			// Image.SCALE_FAST    : 이미지 부드러움보다 속도 우선
			// Image.SCALE_REPLICATE : ReplicateScaleFilter 클래스로 구체화 된 이미지 크기 조절 알고리즘
			// Image.SCALE_SMOOTH  : 속도보다 이미지 부드러움을 우선
			// Image.SCALE_AREA_AVERAGING  : 평균 알고리즘 사용	
			
//			resizedImg = img.getScaledInstance(changeW, changeH, Image.SCALE_AREA_AVERAGING);
//			567ms
//			resizedImg = img.getScaledInstance(changeW, changeH, Image.SCALE_SMOOTH);
//			563ms
//			resizedImg = img.getScaledInstance(changeW, changeH, Image.SCALE_REPLICATE);
//			521ms
//			resizedImg = img.getScaledInstance(changeW, changeH, Image.SCALE_FAST);
//			504ms
			BufferedImage bi = new BufferedImage(changeW, changeH, BufferedImage.TYPE_INT_RGB);
	        Graphics g = bi.getGraphics();
	        g.drawImage(img.getScaledInstance(changeW, changeH, Image.SCALE_AREA_AVERAGING), 0, 0, null);
	        g.dispose();
			return bi;
		} catch (MalformedURLException e) {
			return null;
		} catch (IOException e) {
			return null;
		}
	}
	
}

package threads;

import application.AECController;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import org.jsoup.Jsoup;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;
import utils.BreakException;
import vo.Data;

import java.io.IOException;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class ChromeReader extends Thread {
    private static final String ITEM_REGEX = "\\\"items\\\":\\[.*\\]";
    private static final Pattern ITEMS_JSON_PATTERN = Pattern.compile(ITEM_REGEX);
    private static final int INTERVAL = 60;
    private static final String PAGINATION_INPUT_CSS = "span.next-input.next-large input";
    private static final String PAGINATION_BUTTON_CSS = "span.jump-btn";
    private static ChromeReader instance;
    private final String CURRENCY_REPLACER = "US $";
    private final String PRICE_SPLITOR = " - ";
    private final Pattern PRD_ID_REGEX = Pattern.compile("(\\d+).html");
    private int searchCount = 0;
    private int limitPage = 0;
    private int start = 1;
    private int end = 100;
    private long delay = 1000L;
    private String searchUrl = "https://ko.aliexpress.com/glosearch/api/product?ltype=wholesale&d=y&CatId=0&SearchText=query&trafficChannel=main&SortType=default&page=";
    private String referrerUrl = "https://ko.aliexpress.com/wholesale?ltype=wholesale&d=y&CatId=0&query&trafficChannel=main&SortType=default&page=";
    private String targetUrl;
    private String sortOption;
    private Set<Cookie> cookies;
    private HashSet<String> censorTexts = new HashSet<>();
    private HashSet<String> replaceTexts = new HashSet<>();
    private BlockingQueue<Data> readData = new LinkedBlockingQueue<>();
    private AtomicBoolean alive = new AtomicBoolean(true);
    private AtomicInteger readCount = new AtomicInteger(0);
    private ChromeDriver driver;

    public ChromeReader() {
        setDaemon(true);
        System.setProperty("webdriver.chrome.driver", "src/chromedriver.exe");
        driver = new ChromeDriver(
                // headless option
                // new ChromeOptions().addArguments("headless")
        );
        instance = this;
        driver.manage().window().maximize();
    }

    public static ChromeReader getInstance() {
        return instance;
    }

    private String getSearchUrl(int page) {
        return searchUrl + page;
    }

    @Override
    public void run() {
        if (end > limitPage)
            end = limitPage;
        System.out.println("수집 시작");
        System.out.println("시작 번호 :: " + start + "\t끝 번호 :: " + end + "제한 번호 :: " + limitPage);
        if (end == limitPage && searchCount < limitPage * INTERVAL) {
            AECController.getInstance().setTargetPageCnt((end - start) * INTERVAL + searchCount % INTERVAL);
        } else {
            AECController.getInstance().setTargetPageCnt((end - start + 1) * INTERVAL);
        }
        while (start <= end) {
            String str =
//                        getPageSource(start);
                    getRunParams(start);
            scrapeLists(str);
            start++;
        }
        synchronized (this) {
            System.out.println("수집 종료");
            this.notify();
        }
        exit();
    }

    private void pageNav(int target) {
        try {
            scrollToBottom();
            driver.findElement(By.cssSelector(PAGINATION_INPUT_CSS)).clear();
            driver.findElement(By.cssSelector(PAGINATION_INPUT_CSS)).sendKeys(target + "");
            driver.executeScript("document.querySelector('" + PAGINATION_BUTTON_CSS + "').click();");
            if (driver.findElements(By.cssSelector(PAGINATION_BUTTON_CSS)).size() < 1) {
                throw new NoSuchElementException("알리 익스프레스 오류");
            }
        } catch (NoSuchElementException e) {
            driver.navigate().back();
            pageNav(target);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private String getRunParams(int target) {
        if (target == 1)
            return (String) driver.executeScript("return JSON.stringify(runParams)");
        else {
            String currentUrl = driver.getCurrentUrl();
            if (currentUrl.contains("page=")) {
                String next = currentUrl.replace("page=" + (target - 1), "page=" + target);
                driver.get(next);
            } else {
                driver.get(currentUrl + "&page=" + target);
            }
            delay();
            return (String) driver.executeScript("return JSON.stringify(runParams)");
        }
    }

    private String getPageSource(int target) throws IOException {
        String USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36";

        String url = getSearchUrl(target);

        System.out.println(url);

        return Jsoup
                .connect(url)
                .userAgent(USER_AGENT)
//                .headers()
                .referrer(url.replace("/glosearch/api/product", "/wholesale"))
                .cookies(cookies.stream().collect(Collectors.toMap(Cookie::getName, Cookie::getValue)))
                .ignoreContentType(true)
                .get()
                .body()
                .text();
    }

    private void checkErrorPage(int page) {
        if (!driver.getCurrentUrl().contains(targetUrl)) {
            delay();
            driver.get(targetUrl + "&page=" + page);
            checkErrorPage(page);
        }
    }

    public void exit() {
        alive.set(false);
        if (driver != null) {
            try {
                driver.close();
                driver.quit();
            } catch (WebDriverException ignored) {

            }
        }
    }

    public void networkExceptionAlert() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("네트워크 에러");
        alert.setHeaderText("수집 불가");
        alert.setContentText("알리 익스프레스에서 ip가 차단된 것으로 보입니다. \r\n" +
                "vpn 등을 사용하여 ip를 바꾼 후 재시도 해주세요.");
        alert.showAndWait().ifPresent(rs -> {
            if (rs == ButtonType.OK) {
                alert.close();
            }
        });
        AECController.getInstance().exit();
    }

    public void ready(String searchText) {
        try {
            String TARGET = "https://ko.aliexpress.com";
            driver.get(TARGET);
            driver.findElement(By.cssSelector("input#search-key")).sendKeys(searchText);
            driver.executeScript("document.querySelector('input.search-button').click();");
            String SEARCH_CNT_STRONG_CSS = "div.next-breadcrumb-item > span.next-breadcrumb-text > span";
            Pattern pricePattern = Pattern.compile("\\((\\d+,?)+");
            String searchCntString = driver.findElement(By.cssSelector(SEARCH_CNT_STRONG_CSS))
                    .getText();
            Matcher m = pricePattern.matcher(searchCntString);
            m.find();
            String cntString = m.group();
            searchCount = Integer
                    .parseInt(cntString.replace(",", "").replaceAll("\\(", ""));
            limitPage = searchCount / INTERVAL + 1 >= 100 ? 100 : searchCount / INTERVAL + 1;
            cookies = driver.manage().getCookies();
//            driver.
            targetUrl = driver.getCurrentUrl() + sortOption;
            // callback func
            AECController.getInstance().setTargetCount(searchCount);
            AECController.getInstance().setLimitPage(limitPage + "");
            AECController.getInstance().setStartBtnEnable();

//            searchUrl = searchUrl.replace("query", URLEncoder.encode(searchText, StandardCharsets.UTF_8.toString()));
            searchUrl = searchUrl.replace("query", searchText.replace(' ', '+'));

            // test
        } catch (Exception e) {
            e.printStackTrace();
            delay();
            ready(searchText);
        }
    }

    private void delay() {
        long realDelay = delay + new Random().nextInt(100);
        try {
            Thread.sleep(realDelay);
        } catch (InterruptedException ignored) {

        }
    }

    private void scrollToBottom() throws InterruptedException {
        Long scrollHeight = (Long) driver.executeScript("return document.body.scrollHeight");

        for (int i = 0; i < scrollHeight; i += 200) {
            driver.executeScript("window.scrollTo(0," + i + " )");
            sleep(50);
        }
    }

    private void scrapeLists(String jsonString) {
        JsonParser jsonParser = new JsonParser();
        JsonObject jsonObject = jsonParser.parse(jsonString).getAsJsonObject();

        JsonArray items = jsonObject.getAsJsonArray("items");

        items.forEach(el -> {
            JsonObject item = el.getAsJsonObject();

            AtomicBoolean censored = new AtomicBoolean(true);
            // product name
            String name = item.get("title")
                    .getAsString().toLowerCase();
            try {
                for (String e : censorTexts) {
                    if (name.contains(e)) {
                        censored.set(false);
                        throw new BreakException("금칙어 **" + e + "** 발견");
                    }
                }

                for (String e : replaceTexts) {
                    if (name.contains(e)) {
                        System.out.println("제외어 **" + e + "** 발견. 공백으로 대치.");
                        name = name.replaceAll(e, "");
                    }
                }

                readCount.incrementAndGet();
                if (censored.get()) {
                    Data data = new Data();
                    data.setName(name);
                    // detail page url
                    String url = item.get("productDetailUrl").getAsString();
                    url = url.substring(0, url.indexOf("html") + 4);
                    if (url.startsWith("//"))
                        url = "https:" + url;
                    data.setDetailUrl(url);
                    Matcher m = PRD_ID_REGEX.matcher(url);
                    if (m.find())
                        data.setProductId(m.group(1));
                    // price
                    String priceValue = item.get("price").getAsString();
                    priceValue = priceValue.replace(CURRENCY_REPLACER, "");
                    priceValue = priceValue.replaceAll(",", "");
                    String[] temp = priceValue.split(PRICE_SPLITOR);
                    if (temp.length > 1) {
                        data.setPrice(Double.parseDouble(temp[1]));
                    } else {
                        data.setPrice(Double.parseDouble(temp[0]));
                    }
                    // main img
                    String src = item.get("imageUrl").getAsString();
                    if (!src.startsWith("http"))
                        data.setPicUrl("https:" + src.replace("_220x220xz.jpg", ""));
                    else
                        data.setPicUrl(src.replace("_220x220.jpg", ""));
                    readData.add(data);
                }
            } catch (BreakException e) {
                System.out.println(e.getMessage());
            }
        });

    }

    public double getReadCount() {
        return (double) readCount.get();
    }

    public long getDelay() {
        return delay;
    }

    public void setDelay(long delay) {
        if (delay > 1000)
            this.delay = delay;
    }

    public BlockingQueue<Data> getReadData() {
        return readData;
    }

    public void setReadData(BlockingQueue<Data> readData) {
        this.readData = readData;
    }

    public AtomicBoolean getAlive() {
        return alive;
    }

    public void setAlive(boolean alive) {
        this.alive.set(alive);
    }

    public void setSearchCount(int searchCount) {
        this.searchCount = searchCount;
    }

    public void setLimitPage(int limitPage) {
        this.limitPage = limitPage;
    }

    public void setTargetUrl(String targetUrl) {
        this.targetUrl = targetUrl;
    }

    public void setDriver(ChromeDriver driver) {
        this.driver = driver;
    }

    public double getStart() {
        return (double) start;
    }

    public void setStart(int start) {
        if (start > 1)
            this.start = start;
    }

    public double getEnd() {
        return (double) end - start;
    }

    public void setEnd(int end) {
        if (end < limitPage && end != 0)
            this.end = end;
    }

    public void setSortOption(String sortOption) {
        this.sortOption = sortOption;
    }

    public void setCensorTexts(String censorString) {
        for (String text : censorString.split(",")) {
            text = text.trim();
            if (!text.isEmpty())
                censorTexts.add(text.toLowerCase());
        }
    }

    public void setReplaceTexts(String replaceString) {
        for (String text : replaceString.split(",")) {
            text = text.trim();
            if (!text.isEmpty())
                replaceTexts.add(text.toLowerCase());
        }
    }
}

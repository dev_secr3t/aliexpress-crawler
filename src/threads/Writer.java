package threads;

import application.AECController;
import org.apache.commons.io.FileUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellAddress;
import utils.BreakException;
import utils.RandomString;
import vo.Data;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.*;
import java.util.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public class Writer extends Thread {

	public static HashSet<String> wroteDatas = new HashSet<String>();
	private static boolean useNaver = true;
	private static boolean useSmelch = true;
	private static Writer instance;
	private final String[] replaces = {"\\\\", "[?]", "\"", "<", ">", "/"};
	private String userProductName = "";
	private String header;
	private String footer;
	private String afterServiceContent;
	private String afterServicePhoneNumber;
	private String categoryID;
	private int conversionValue;
	private AtomicInteger wroteCount = new AtomicInteger(0);
	private AtomicBoolean crawlerAlive;
	private boolean writerAlive = true;
	private BlockingQueue<Data> crawledData;
	private Map<Integer, String> naverStaticValues = new HashMap<>();
	private Map<Integer, String> smelchStaticValues = new HashMap<>();
	private LinkedHashMap<Double, Integer> priceMap = new LinkedHashMap<>();
	private Data lastData = null;
	private int prev = -1;

	public Writer() {
		setValues();
		setPriceMap();
		setDaemon(true);
		instance = this;
	}

	public static Writer getInstance() {
		return instance;
	}

	public static void toggleNaver() {
		useNaver = !useNaver;
	}

	public static void toggleSmelch() {
		useSmelch = !useSmelch;
	}

	public static boolean useNaver() {
		return useNaver;
	}

	public static boolean useSmelch() {
		return useSmelch;
	}

	@Override
	public void run() {
		while (crawlerAlive.get() || !crawledData.isEmpty()) {
			synchronized (Crawler.getInstance()) {
				try {
					if (crawledData.isEmpty() && Crawler.getInstance().alive().get()) {
						Crawler.getInstance().wait();
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			Data d = crawledData.poll();
			if (d != null) {
				if (d.dir != prev) {
					prev = d.dir;
					if (lastData != null)
						addBoundaries(lastData);
					addBoundaries(d);
					copyExcel();
				}
				writeExcel(d);
				wroteDatas.add(d.getMainImg());
				wroteCount.getAndIncrement();
				lastData = d;
			}
			synchronized (this) {
				notify();
			}
		}
		addBoundaries(lastData);
		writerAlive = false;
	}

	public void delay(long delay) {
		try {
			Thread.sleep(delay);
		} catch (InterruptedException e) {
		}
	}

	public void copyExcel() {
		String path = "output/" + prev + "/";
		new File(path).mkdirs();
		FileSystem system = FileSystems.getDefault();
		if (useSmelch && prev < 1) {
			File smelchOriginFile = new File("setting/smelch.xls");
			File smelchTargetFile = new File(path + "smelch.xls");
			Path smelchOrigin = system.getPath(smelchOriginFile.getPath());
			Path smelchTarget = system.getPath(smelchTargetFile.getPath());
			try {
				Files.copy(smelchOrigin, smelchTarget, StandardCopyOption.REPLACE_EXISTING);
			} catch (IOException e) {
				System.out.println("File Error");
			}
		}
		if (useNaver) {
			File naverOriginFile = new File("setting/naver.xls");
			File naverTargetFile = new File(path + "naver.xls");
			Path naverOrigin = system.getPath(naverOriginFile.getPath());
			Path naverTarget = system.getPath(naverTargetFile.getPath());
			try {
				Files.copy(naverOrigin, naverTarget, StandardCopyOption.REPLACE_EXISTING);
			} catch (IOException e) {
				System.out.println("File Error");
			}
		}
	}

	public void writeExcel(Data d) {
		System.out.println("엑셀 저장");
		triming(d);
		setPrice(d);
		if (useNaver)
			writeNaver(d);
		if (useSmelch)
			writeSmelch(d);
	}

	public void writeSmelch(Data d) {
		try {
			String target = "output/0/smelch.xls"; // smelch 엑셀은 한 곳에 저장해달라는 요청
			FileInputStream fis = new FileInputStream(target);
			Workbook workbook = new HSSFWorkbook(fis);
			Row row = getNextRow(workbook, false);
			setSmelchCellValue(row, d);
			saveExcel(target, workbook);
		} catch (IOException e) {
		}
	}

	public void writeNaver(Data d) {
		try {
			String target = "output/" + prev + "/naver.xls";
			FileInputStream fis = new FileInputStream(target);
			Workbook workbook = new HSSFWorkbook(fis);
			Row row = getNextRow(workbook, true);
			setNaverCellValue(row, d);
			saveExcel(target, workbook);
			fis.close();
		} catch (IOException e) {
		}
	}

	public Row getNextRow(Workbook workbook, boolean isNaver) {
		Sheet sheet = workbook.getSheetAt(0);
		if (isNaver) {
			Row row = sheet.createRow(sheet.getLastRowNum() + 1);
			sheet.setActiveCell(new CellAddress(sheet.getLastRowNum() + 1, 0));
			return row;
		} else {
			Row row = sheet.createRow(wroteCount.get() + 1);
			sheet.setActiveCell(new CellAddress(wroteCount.get() + 1, 0));
			return row;
		}
	}

	public void addBoundaries(Data d) {
		String boundary = d.getMainImg().split("-")[0];
		AECController.Boundaries.add(boundary);
	}

	public void setNaverCellValue(Row row, Data d) {
		String name = d.getName();
		if (name.length() > 30) {
			name = name.substring(0, new Random().nextInt(5) + 26);
		}
		createAndSetCellValue(row, NaverCellValue.카테고리ID.getVal(), categoryID);
		createAndSetCellValue(row, NaverCellValue.상품명.getVal(),
				userProductName + "\r\n" + RandomString.getRandomString(3) + "\r\n" + name);
		createAndSetCellValue(row, NaverCellValue.상품상세.getVal(), d.getDetails());
		createAndSetCellValue(row, NaverCellValue.판매가.getVal(), (int) d.getPrice());
		createAndSetCellValue(row, NaverCellValue.대표이미지.getVal(), d.getMainImg());
		createAndSetCellValue(row, NaverCellValue.중간이미지.getVal(), d.getMainImg());
		createAndSetCellValue(row, NaverCellValue.옵션명.getVal(), d.getOptionNames());
		createAndSetCellValue(row, NaverCellValue.옵션값.getVal(), d.getOptionValues());
		createAndSetCellValue(row, NaverCellValue.상품번호.getVal(), d.getProductId());
		createAndSetCellValue(row, NaverCellValue.AS안내내용.getVal(), afterServiceContent);
		createAndSetCellValue(row, NaverCellValue.AS전화번호.getVal(), afterServicePhoneNumber);
		naverStaticValues.forEach((k, v) -> {
			try {
				if (k == 19)
					createAndSetCellValue(row, k, v);
				else
					createAndSetCellValue(row, k, Integer.parseInt(v));
			} catch (NumberFormatException e) {
				createAndSetCellValue(row, k, v);
			}
		});
	}

	public void setSmelchCellValue(Row row, Data d) {
		String name = d.getName();
		int price = (int) Math.floor(d.getPrice() / 1.3 / 100) * 100;
		if (name.length() > 30) {
			name = name.substring(0, new Random().nextInt(5) + 26);
		}
		createAndSetCellValue(row, 1, userProductName);
		createAndSetCellValue(row, 5, d.getProductId());
		createAndSetCellValue(row, 6, userProductName + "\r\n" + RandomString.getRandomString(3) + "\r\n" + name);
		createAndSetCellValue(row, 32, d.getDetails());
		createAndSetCellValue(row, 14, price);
		createAndSetCellValue(row, 15, (int) Math.floor(price * 0.92 / 100) * 100);
		createAndSetCellValue(row, 31, d.getPicUrl()); // 대표이미지
		int optNameCol = 17;
		try {
			for (String optName : d.getOptionNames().split("\r\n")) {
				createAndSetCellValue(row, optNameCol, optName);
				optNameCol += 2;
			}
			int optValueCol = 18;
			for (String optionValue : d.getOptionValues().split("\r\n")) {
				createAndSetCellValue(row, optValueCol, optionValue);
				optValueCol += 2;
			}
			createAndSetCellValue(row, 16, 1);
		} catch (NullPointerException e) {
//			createAndSetCellValue(row, 16, 0);
		}
		if (row.getCell(17).getStringCellValue().isEmpty() && row.getCell(18).getStringCellValue().isEmpty()) {
			createAndSetCellValue(row, 16, 0);
		}

		smelchStaticValues.forEach((k, v) -> {
			try {
				if (v.startsWith("0") && v.length() < 3)
					createAndSetCellValue(row, k, v);
				else
					createAndSetCellValue(row, k, Integer.parseInt(v));
			} catch (NumberFormatException e) {
				createAndSetCellValue(row, k, v);
			}
		});

	}

	public void saveExcel(String target, Workbook workbook) throws IOException {
		FileOutputStream fos = new FileOutputStream(target);
		workbook.write(fos);
		workbook.close();
		fos.flush();
		fos.close();
	}

	public void createAndSetCellValue(Row row, int cellNum, String cellValue) {
		if (cellValue.length() > 32760)
			cellValue = cellValue.substring(0, 32760);
		row.createCell(cellNum).setCellValue(cellValue);
	}

	public void createAndSetCellValue(Row row, int cellNum, int cellValue) {
		row.createCell(cellNum).setCellValue(cellValue);
	}

	public String trimString(String s) {
		if (s == null)
			return "";
		String str = s;
		for (String rp : replaces) {
			str = str.replaceAll(rp, "");
		}
		str = str.replaceAll("[*]", "x");

		return str;
	}

	public void triming(Data d) {
		String optStr = d.getOptionValues();
		optStr = trimString(optStr);
		d.setOptionValues(optStr);

		String name = d.getName();
		name = trimString(name);

		String detail = d.getDetails();
		d.setDetails(header + detail + footer);
		d.setName(name);
	}

	public void setValues() {
		naverStaticValues.put(0, "신상품");
		naverStaticValues.put(4, "1000");
		naverStaticValues.put(16, "과세상품");
		naverStaticValues.put(17, "Y");
		naverStaticValues.put(18, "Y");
		naverStaticValues.put(19, "0200037");
		naverStaticValues.put(20, "중국 및 홍콩 등");
		naverStaticValues.put(21, "Y");
		naverStaticValues.put(23, "택배, 소포, 등기");
		naverStaticValues.put(24, "무료");
		naverStaticValues.put(26, "착불");
		naverStaticValues.put(29, "30000");
		naverStaticValues.put(30, "60000");
		naverStaticValues.put(33, "해외구매대행제품입니다. 배송기간 15일이상 소용됩니다.");
		naverStaticValues.put(34, "23");
		naverStaticValues.put(35, "%");
		naverStaticValues.put(36, "3");
		naverStaticValues.put(37, "개");
		naverStaticValues.put(38, "2000");
		naverStaticValues.put(39, "원");
//		naverStaticValues.put(42, "100");
//		naverStaticValues.put(43, "200");
		naverStaticValues.put(49, "단독형");
		naverStaticValues.put(62, "N");

		// smelch
		smelchStaticValues.put(0, "asdfasd111");
		smelchStaticValues.put(7, "중국");
		smelchStaticValues.put(9, "중국");
		smelchStaticValues.put(10, "0");
		smelchStaticValues.put(11, "1");
		smelchStaticValues.put(12, "01");
		smelchStaticValues.put(13, "1");
		smelchStaticValues.put(23, "01");
		smelchStaticValues.put(24, "01");
		smelchStaticValues.put(25, "01");
		smelchStaticValues.put(28, "4000");
		smelchStaticValues.put(29, "30000");
		smelchStaticValues.put(30, "60000");
	}

	public void setPrice(Data d) {
		double originPrice = d.getPrice();
		try {
			priceMap.forEach((k, v) -> {
				if (originPrice < k) {
					d.setPrice(Math.floor((originPrice * conversionValue + v) * 1.3 / 100) * 100);
					throw new BreakException("break");
				}
			});
		} catch (BreakException e) {
		}
	}

	public void setPriceMap() {
		try {
			String data[] = FileUtils.readFileToString(new File("setting/price.txt"), "UTF-8").split("\r\n");
			for (String i : data) {
				String[] e = i.split(":");
				priceMap.put(Double.parseDouble(e[0]), Integer.parseInt(e[1]));
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NumberFormatException e) {

		}

	}

	public void setCrawledData(BlockingQueue<Data> crawledData) {
		this.crawledData = crawledData;
	}

	public void setHeader(String header) {
		this.header = header;
	}

	public void setFooter(String footer) {
		this.footer = footer;
	}

	public void setAfterServiceContent(String afterServiceContent) {
		this.afterServiceContent = afterServiceContent;
	}

	public void setAfterServicePhoneNumber(String afterServicePhoneNumber) {
		this.afterServicePhoneNumber = afterServicePhoneNumber;
	}

	public void setConversionValue(int conversionValue) {
		this.conversionValue = conversionValue;
	}

	public void setCrawlerAlive(AtomicBoolean crawlerAlive) {
		this.crawlerAlive = crawlerAlive;
	}

	public double getWroteCount() {
		return (double) wroteCount.get();
	}

	public AtomicInteger wroteCount() {
		return wroteCount;
	}

	public void setCategoryID(String categoryID) {
		this.categoryID = categoryID;
	}

	public void setUserProductName(String userProductName) {
		this.userProductName = userProductName;
	}

	public boolean isWriterAlive() {
		return writerAlive;
	}

	private enum NaverCellValue {
		카테고리ID(1), 상품명(2), 판매가(3), AS안내내용(5), AS전화번호(6), 대표이미지(7), 중간이미지(8), 상품상세(9), 상품번호(10), 옵션명(50), 옵션값(51);
		private int val;

		private NaverCellValue(int val) {
			this.val = val;
		}

		public int getVal() {
			return val;
		}
	}
}

package threads;

import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.openqa.selenium.JavascriptException;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import utils.AliScript;
import vo.Data;

import java.io.IOException;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Pattern;

public class Crawler extends Thread {


    private static final ChromeDriver CHROME_DRIVER;

    private static Crawler instance;

    static {
        CHROME_DRIVER = new ChromeDriver(
                new ChromeOptions().addArguments("--headless", "--disable-gpu", "--blink-settings=imagesEnabled=false")
        );
    }

    private final String USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36";
    private final String DESC_REGEX = "descriptionUrl:\"(.*)\";";
    private final Pattern DESC_PATTERN = Pattern.compile(DESC_REGEX);
    private long delay = 1000L;
    private String logo = "";
    private AtomicInteger crawledCount = new AtomicInteger(0);
    private BlockingQueue<Data> passedData;
    private BlockingQueue<Data> crawledData;
    private AtomicBoolean differAlive;
    private AtomicBoolean alive = new AtomicBoolean(true);

    public Crawler() {
        setDaemon(true);
        instance = this;
    }

    public static Crawler getInstance() {
        return instance;
    }

    public double getCrawledCount() {
        if (crawledCount.get() == 0)
            return 1.0d;
        else
            return (double) crawledCount.get();
    }

    @Override
    public void run() {
        synchronized (ImageDiffer.getInstance()) {
            try {
//				ImageDiffer.getInstance().wait(1000 * 60 * 5);
                ImageDiffer.getInstance().wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        while (!passedData.isEmpty() || differAlive.get()) {
            Data d = null;
            try {
                d = passedData.poll();
                if (d != null) {
                    crawl(d, 0);
                    synchronized (this) {
                        notify();
                    }
                    crawledCount.incrementAndGet();
                    delay();
                }
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
                d.setName("오류상품");
                crawledCount.incrementAndGet();
            }
            synchronized (ImageDiffer.getInstance()) {
                try {
                    if (passedData.isEmpty() && ImageDiffer.getInstance().getAlive().get()) {
                        ImageDiffer.getInstance().wait();
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        alive.set(false);
        synchronized (this) {
            notify();
        }
    }

    private void crawl(Data d, int trying) throws IOException, InterruptedException {
        String script = "try { return runParams; } catch (e) { return null; }";
        boolean hasError = true;
        if (trying > 3)
            return;
        System.out.println("상품 수집");

        CHROME_DRIVER.get(d.getDetailUrl());

        for (int i = 0; i < 10; i++) {
            if (Objects.nonNull(CHROME_DRIVER.executeScript(script))) {
                hasError = false;
                break;
            }
            sleep(400);
        }

        if (hasError) {
            delay();
            crawl(d, trying++);
        }

        setOptionAndDetail(d);
    }

    private void setOptionAndDetail(Data d) throws IOException {

        try {
            String descUrl = (String) CHROME_DRIVER.executeScript(AliScript.GET_DESCRIPTION_URL);
            String optionImgs = "";
            d.setPrice(Double.parseDouble((String) CHROME_DRIVER.executeScript(AliScript.GET_PRICE_SCRIPT)));
            Map<String, String> propertyMap = (Map<String, String>) CHROME_DRIVER.executeScript(AliScript.GET_PROPERTY_MAP);

            d.setOptionNames(propertyMap.get("optionNames"));
            d.setOptionValues(propertyMap.get("optionValues"));
            optionImgs += propertyMap.get("optionImgs");
            String desc = getDesc(descUrl, 0);

            if(desc.isEmpty())
                return;

            String details = desc + optionImgs;

            String replacedDetails = details.replaceAll("src=\"//", "src=\"https://");

            d.setDetails(replacedDetails);
            crawledData.add(d);
        } catch (JavascriptException ignored) {

        }

    }

    private String getDesc(String url, int cnt) throws IOException {
        String result = "";
        cnt++;
        try {
            String USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36";
            Document doc = Jsoup.connect(url).userAgent(USER_AGENT).get();
            Elements descImgs = doc.select("img");
            String BEGIN_P = "<p align='center'>";
            String END_P = "</p>";
            result = BEGIN_P + descImgs.toString() + END_P;
        } catch (IllegalArgumentException e) {
            if (cnt < 4) {
                delay();
                return getDesc(url, cnt);
            } else
                System.out.println("getDesc(d,url,cnt) : 세부페이지 설정 도중 오류가 발생하였습니다. " + url);
        } catch(HttpStatusException e) {
            System.out.println("tDesc(d,url,cnt) : 세부페이지가 존재하지 않습니다.");
        }
        return result;
    }

    private void delay() {
        try {
            Thread.sleep(delay);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public BlockingQueue<Data> getCrawledData() {
        return crawledData;
    }

    public void setCrawledData(BlockingQueue<Data> crawledData) {
        this.crawledData = crawledData;
    }

    public AtomicBoolean alive() {
        return alive;
    }

    public void setPassedData(BlockingQueue<Data> passedData) {
        this.passedData = passedData;
    }

    public void setDelay(long delay) {
        if (delay > 1000)
            this.delay = delay;
    }

    public void setDifferAlive(AtomicBoolean differAlive) {
        this.differAlive = differAlive;
    }

    public void setLogo(String logoSrc) {
        if (!logoSrc.isEmpty())
            this.logo = "<img src = '" + logoSrc + "' style='width:50px; height:50px;'/>";
    }

    public void exit() {
        alive.set(false);
        if (CHROME_DRIVER != null) {
            try {
                CHROME_DRIVER.close();
                CHROME_DRIVER.quit();
            } catch (WebDriverException ignored) {

            }
        }
    }
}

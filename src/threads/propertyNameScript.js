var propertyName = '';
runParams
    .data
    .skuModule
    .productSKUPropertyList
    .forEach(e = > {
    var name = e.skuPropertyName;
if (name != 'Ships from')
    propertyName += name + ','
})
;

var priceString = runParams.data.priceModule.formatedActivityPrice || runParams.data.priceModule.formatedPrice;
return priceString.match(/(\d+\.\d+)?( - )?(\d+\.\d+)/)[3];

var result = {};
var optionNames = '';
var optionValues = '';
var optionImgs = '';
let beginP = '<p align=\'center\'>';
let endP = '</p>';
let logo = '';
let beginSpan = '<span style=\'font-size : 25px; color : brown\'>';
let endSpan = '</span>'
let beginStrong = '<strong>';
let endStrong = '</strong>';


runParams
    .data
    .skuModule
    .productSKUPropertyList
    .forEach(e = > {
    if(e.skuPropertyName !== 'Ships From'
)
{
    optionNames += e.skuPropertyName + '\r\n';
    var num = 1;
    e.skuPropertyValues.forEach(value = > {
        var optionStr = value.propertyValueDisplayName || value.propertyValueName;
    if (optionStr.length > 25) {
        optionStr = optionStr.substr(0, 25);
    }
    var valueString =
        num +
        ' - ' +
        optionStr +
        ',';
    optionValues += valueString;
    var imgUrl = value.skuPropertyImagePath;
    if (imgUrl) {
        var img = document.createElement('IMG');
        img.src = imgUrl;
        optionImgs +=
            beginP +
            logo +
            beginSpan +
            beginStrong +
            num +
            endStrong +
            ' - ' +
            optionStr +
            endSpan +
            endP +
            beginP +
            img.outerHTML +
            endP +
            "<br/><br/>"
    }
    num++;
})
    ;
    optionValues = optionValues.replace(/,$/, '');
    optionValues += '\r\n';
}
})
;

result.optionNames = optionNames;
result.optionValues = optionValues;
result.optionImgs = optionImgs;

return result;
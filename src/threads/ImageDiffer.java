package threads;

import application.AECController;
import utils.BreakException;
import utils.ImageDiff;
import utils.ImageResize;
import utils.WatermarkUtil;
import vo.Data;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Stack;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public class ImageDiffer extends Thread {

	private String imageDiffPath;
	private String imageSavePath;
	private String date;
	private AtomicInteger passedCount = new AtomicInteger(0);
	private AtomicInteger imgNumber;
	private AtomicBoolean readerAlive;
	private AtomicBoolean alive = new AtomicBoolean(true);
	private BlockingQueue<Data> readData;
	private BlockingQueue<Data> passedData;
	private HashSet<String> imgs = new HashSet<>();
	private Stack<String> errorImgs = new Stack<>();
	private BufferedImage watermarkImage;
	private static ImageDiffer instance;
	private boolean prevErr = false;
	
	@Override
	public void run() {
		addAllImgsToSet();
		synchronized (ChromeReader.getInstance()) {
			try {
//				ChromeReader.getInstance().wait(1000 * 60 * 5);
				ChromeReader.getInstance().wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		System.out.println("ChromeReader 종료");
		new File(imageDiffPath + "\\check").mkdirs();
		System.out.println("준비된 이미지 갯수 :: " + imgs.size());
		System.out.println("판별할 상품 수 :: " + readData.size());
		while (!readData.isEmpty() || readerAlive.get()) {
			Data d = readData.poll();
			if (d != null) {
				if (passedCount.get() % 500 == 0) {
					makeDir();
					imageSavePath = "output/" + passedCount.get() / 500;
				}
				try {
					BufferedImage image_640 = ImageResize.resize(d.getPicUrl(), 640.0d);
					BufferedImage image_100 = ImageResize.resize(d.getPicUrl(), 100.0d);
					long before = System.currentTimeMillis();
					if (image_100 != null) {

//						빠른 테스트를 위해서 중복검출 통과
						for (String imgName : imgs) {
							String absoluteImgPath = imageDiffPath + "\\check\\" + imgName;
							if (!new File(absoluteImgPath).exists()) {
								BufferedImage bi;
								try {
									bi = ImageResize.resizeWithFile(imageDiffPath + "\\" + imgName, 100.0d);
								} catch (NullPointerException e) {
									System.out.println("오류 이미지 발견, 검사 목록에서 제외");
									errorImgs.push(imgName);
									continue;
								}
								try {
									saveImg(imageDiffPath + "\\check", imgName, bi);
								} catch (IOException e) {
									e.printStackTrace();
								}
							}
							double diffPercent = ImageDiff.getDifferencePercent(absoluteImgPath, image_100);
							if (diffPercent < 5.00) {
								throw new BreakException("same img found :: " + imgName);
							}
						}

						while (!errorImgs.empty()) {
							imgs.remove(errorImgs.pop());
						}
						d.dir = passedCount.get() / 500;
						passedCount.getAndIncrement();
						String name = getNextImgName(d);
						d.setMainImg(name);
						passedData.add(d);
						System.out.println(
								"PassedTime :: " + (double) (System.currentTimeMillis() - before) / 1000 + " seconds");
						synchronized (this) {
							notify();
						}
						synchronized (Writer.getInstance()) {
							try {
//								Writer.getInstance().wait(1000 * 60 * 5);
								Writer.getInstance().wait();
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
						}
						if (Writer.wroteDatas.contains(d.getMainImg())) {
//							BufferedImage saveImg = ImageResize.resize(d.getPicUrl(), 640.0d);
							BufferedImage watermarkedImg = WatermarkUtil.watermark(image_640, watermarkImage);
							saveImg(imageSavePath, name, watermarkedImg);
							saveImg(imageDiffPath, name, watermarkedImg);
							imgs.add(name);
							Writer.wroteDatas.remove(d.getMainImg());
							prevErr = false;
						}
					}
				} catch (BreakException e) {
					System.out.println(e.getMessage());
					continue;
					// 같은 이미지 발견
				} catch (IOException e) {
					e.printStackTrace();
				} catch (NullPointerException e) {
					if(!prevErr)
						Writer.getInstance().wroteCount().decrementAndGet();
					System.out.println("이미지 저장중 오류 발생 엑셀에서 해당 값을 다음 데이터로 덮어씌웁니다.");
					prevErr = true;
				} catch (Exception e) {
					if(!prevErr)
						Writer.getInstance().wroteCount().decrementAndGet();
					e.printStackTrace();
					System.out.println("이미지 저장중 알 수 없는 오류가 발생하여 엑셀에서 해당 값을 다음 데이터로 덮어씌웁니다.");
					prevErr = true;
				}
			}

//			reduce cpu usage
		}
		alive.set(false);
		synchronized (this) {
			notify();
		}
	}

	public void delay(long delay) {
		try {
			Thread.sleep(delay);
		} catch (InterruptedException e) {
		}
	}

	public String getNextImgName(Data d) {
		String result = "";
		String imgText = imgNumber.getAndIncrement() + "";
		int len = imgText.length();
		if (len < 9) {
			for (int i = 0, limit = 9 - len; i < limit; i++) {
				imgText = "0" + imgText;
			}
		}
		result = imgText + "-" + date + "-" + d.getProductId() + ".jpg";
		AECController.getInstance().setImgNum(imgText);
		return result;
	}

	public void addAllImgsToSet() {
		if (imageDiffPath != null) {
			File imgDiffDir = new File(imageDiffPath);
			try {
				imgs.addAll(Arrays.asList(imgDiffDir.list((File dir, String name) -> {
					return name.endsWith("jpg");
				})));
			} catch (NullPointerException npe) {
				System.out.println("설정된 이미지경로에 폴더가 없으므로 폴더를 생성합니다.");
				imgDiffDir.mkdirs();
				addAllImgsToSet();
			}
		}
	}

	public void saveImg(String path, String name, BufferedImage img) throws IOException {
		if (path != null && name != null && img != null) {
			try (OutputStream os = new FileOutputStream(new File(path + "\\" + name))) {
				ImageIO.write(img, "jpg", os);
			}
		}
	}

	public void makeDir() {
		File outputDir = new File("output/" + passedCount.get() / 500);
		outputDir.mkdirs();
	}

	public ImageDiffer() {
		date = LocalDate.now().toString();
		setDaemon(true);
		loadWatermark();
		instance = this;
	}

	public double getPassedCount() {
		if (passedCount.get() == 0)
			return 1.0d;
		else
			return (double) passedCount.get();
	}

	public void setReadData(BlockingQueue<Data> readData) {
		this.readData = readData;
	}

	public BlockingQueue<Data> getPassedData() {
		return passedData;
	}

	public AtomicBoolean getAlive() {
		return alive;
	}

	public void setReaderAlive(AtomicBoolean readerAlive) {
		this.readerAlive = readerAlive;
	}

	public void setImgNumber(AtomicInteger imgNumber) {
		this.imgNumber = imgNumber;
	}

	public void setImageDiffPath(String imageDiffPath) {
		this.imageDiffPath = imageDiffPath;
	}

	public void setPassedData(BlockingQueue<Data> passedData) {
		this.passedData = passedData;
	}

	public void loadWatermark() {
		try {
			watermarkImage = ImageIO.read(new File("setting/watermark.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static ImageDiffer getInstance() {
		return instance;
	}
}

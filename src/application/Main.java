package application;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;


public class Main extends Application {
	
	private static Stage stage;
	
	@Override
	public void start(Stage primaryStage) {
		try {
            AnchorPane root = FXMLLoader.load(getClass().getResource("ali.fxml"));
			Scene scene = new Scene(root);
			primaryStage.setScene(scene);
			primaryStage.show();
			primaryStage.setResizable(false);
			stage = primaryStage;
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public static void restart() {
		stage.close();
        Platform.runLater(() -> new Main().start(new Stage()));
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}

package application;

import customField.NumericField;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.paint.Color;
import javafx.stage.DirectoryChooser;
import org.apache.commons.io.FileUtils;
import threads.ChromeReader;
import threads.Crawler;
import threads.ImageDiffer;
import threads.Writer;
import vo.Data;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.*;
import java.util.LinkedHashSet;
import java.util.ResourceBundle;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class AECController implements Initializable {
	@FXML
	TextField searchTF;
	@FXML
	TextField categoryTF;
	@FXML
	TextField userPNTF;
	@FXML
	NumericField startF;
	@FXML
	NumericField endF;
	@FXML
	NumericField moneyF;
	@FXML
	NumericField readDelayF;
	@FXML
	NumericField crawlDelayF;
	@FXML
	TextArea a_sA;
	@FXML
	TextField a_sF;
	@FXML
	TextArea headerA;
	@FXML
	TextArea footerA;
	@FXML
	TextField imgF;
	@FXML
	Label imgPathStatus;
	@FXML
	Label searchedProduct;
	@FXML
	Label limitPage;
	@FXML
	ProgressBar readProgress;
	@FXML
	ProgressBar crawlProgress;
	@FXML
	ProgressBar writeProgress;
	@FXML
	Button startBtn;
	@FXML
	Button readyBtn;
	@FXML
	Button restartBtn;
	@FXML
	Label crawledLinkCnt;
	@FXML
	Label targetLinkCnt;
	@FXML
	Label passedDataCnt;
	@FXML
	Label crawledDataCnt_1;
	@FXML
	Label crawledDataCnt_2;
	@FXML
	Label wroteDataCnt;
	@FXML
	TextField logoSrc;
	@FXML
	RadioButton sortDefault;
	@FXML
	RadioButton sortBought;
	@FXML
	RadioButton sortNew;
	@FXML
	RadioButton sortPriceAsc;
	@FXML
	RadioButton sortPriceDesc;
	@FXML
	TextArea censorA;
	@FXML
	TextArea replaceA;
	@FXML
	Label leftCount;

	private double targetPageCnt = 0.0d;
	private File imgPath;
	private static final String CONF_PATH = "setting/";
	private static final String HEADER = "header.txt";
	private static final String FOOTER = "footer.txt";
	private static final String AFTERSC = "after_service_content.txt";
	private static final String AFTERSP = "after_service_ph.txt";
	private static final String IMGNUM = "imgNum.txt";
	private static final String IMGPATH = "imgPath.txt";
	private static final String LOGO_SRC = "logoSrc.txt";
	private static final String CENSORS = "censor.txt";
	private static final String REPLACES = "replace.txt";

	public static final LinkedHashSet<String> Boundaries = new LinkedHashSet<>();
	private AtomicInteger imgNum = new AtomicInteger(0);

	private LinkedBlockingQueue<Data> readData = new LinkedBlockingQueue<>();
	private LinkedBlockingQueue<Data> passedData = new LinkedBlockingQueue<>();
	private LinkedBlockingQueue<Data> crawledData = new LinkedBlockingQueue<>();

	private ChromeReader reader;
	private Crawler crawler;
	private Writer writer;
	private ImageDiffer differ;
	// SingleTon
	private static AECController instance;

	public AECController() {
		instance = this;
	}

	public static AECController getInstance() {
		return instance;
	}

	/*
	 * initialize method area
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		makeDir();
		reader = new ChromeReader();
		crawler = new Crawler();
		writer = new Writer();
		differ = new ImageDiffer();
		checkConfs();
	}

	public void checkConfs() {
		checkHeader();
		checkFooter();
		checkAfterServiceContent();
		checkAfterServicePh();
		checkImgNum();
		checkImgPath();
		checkLogoSrc();
		checkCensorTxt();
		checkReplaceTxt();
	}

	public void checkHeader() {
		headerA.setText(readFile(new File(CONF_PATH + HEADER)));
	}

	public void checkFooter() {
		footerA.setText(readFile(new File(CONF_PATH + FOOTER)));
	}

	public void checkAfterServiceContent() {
		a_sA.setText(readFile(new File(CONF_PATH + AFTERSC)));
	}

	public void checkAfterServicePh() {
		a_sF.setText(readFile(new File(CONF_PATH + AFTERSP)));
	}

	public void checkLogoSrc() {
		logoSrc.setText(readFile(new File(CONF_PATH + LOGO_SRC)));
	}

	public void checkCensorTxt() {
		censorA.setText(readFile(new File(CONF_PATH + CENSORS)));
	}

	public void checkReplaceTxt() {
		replaceA.setText(readFile(new File(CONF_PATH + REPLACES)));
	}

	public void checkImgNum() {
		String imgText = readFile(new File(CONF_PATH + IMGNUM));
		if (imgText.isEmpty())
			imgNum.set(0);
		else
			imgNum.set(Integer.parseInt(imgText));
		imgF.setText(stringNum(imgText));
	}

	public String stringNum(String text) {
//		if len < 9 fills with 0
		int len = text.length();
		if (len < 9) {
			for (int i = 0, limit = 9 - len; i < limit; i++) {
				text = "0" + text;
			}
		}
		return text;
	}

	public void checkImgPath() {
		String imgDiffPath = readFile(new File(CONF_PATH + IMGPATH));
		if (!imgDiffPath.isEmpty()) {
			imgPath = new File(imgDiffPath);
			System.out.println(imgPath.getAbsolutePath());
			imgPathStatus.setText("O");
			imgPathStatus.setTextFill(Color.web("#9048ff"));
		}
	}

	public void makeDir() {
		File targetDir = new File("output");
		if (!targetDir.exists())
			targetDir.mkdirs();
	}

	/*
	 * start method area
	 */
	public void start() {
		if (!Writer.useNaver() && !Writer.useSmelch()) {
			cannotStartAlert();
		} else {
			// reader
			readerInitialize();
			// differ
			differInitialize();
			// crawler
			crawlerInitialize();
			// writer
			writerInitialize();
			reader.start();
			differ.start();
			crawler.start();
			writer.start();
			setProgress();
			disableStartBtn();
			disableReadyBtn();
		}
	}

	public void disableStartBtn() {
		Platform.runLater(() -> {
			startBtn.setDisable(true);
		});
	}

	public void disableReadyBtn() {
		Platform.runLater(() -> {
			readyBtn.setDisable(true);
		});
	}

	/*
	 * exit method area
	 */
	public void exit() {
		saveConfs();
		reader.exit(); // thread 를 daemon으로 설정해도 main thread가 종료될 때,
		crawler.exit();
		Platform.runLater(() -> {
			Platform.exit();
			Runtime.getRuntime().halt(0);
		});
	}

	public void saveConfs() {
		saveAfterServiceContent();
		saveAfterServicePh();
		saveFooter();
		saveHeader();
		saveImgNum();
		saveImgPath();
		saveLogoSrc();
		saveCensorTxt();
		saveReplaceTxt();
	}

	public void saveHeader() {
		saveFile(new File(CONF_PATH + HEADER), headerA.getText());
	}

	public void saveFooter() {
		saveFile(new File(CONF_PATH + FOOTER), footerA.getText());
	}

	public void saveAfterServiceContent() {
		saveFile(new File(CONF_PATH + AFTERSC), a_sA.getText());
	}

	public void saveAfterServicePh() {
		saveFile(new File(CONF_PATH + AFTERSP), a_sF.getText());
	}

	public void saveImgNum() {
		saveFile(new File(CONF_PATH + IMGNUM), imgNum.get() + "");
	}

	public void saveImgPath() {
		if (imgPath != null)
			saveFile(new File(CONF_PATH + IMGPATH), imgPath.getAbsolutePath());
	}

	public void saveLogoSrc() {
		saveFile(new File(CONF_PATH + LOGO_SRC), logoSrc.getText());
	}

	public void saveCensorTxt() {
		saveFile(new File(CONF_PATH + CENSORS), censorA.getText());
	}

	private void saveReplaceTxt() {
		saveFile(new File(CONF_PATH + REPLACES), replaceA.getText());
	}

	/*
	 * reader method area
	 */
	public void readyReader() {
		deleteAllOutput();
		setSortOption();
		reader.setCensorTexts(censorA.getText());
		reader.setReplaceTexts(replaceA.getText());
		reader.ready(searchTF.getText());
	}

	public void readerInitialize() {
		setReaderDelay();
		setPage();
		setReaderReadData();
	}

	public void setReaderDelay() {
		reader.setDelay(readDelayF.getValue() * 1000l);
	}

	public void setPage() {
		reader.setStart(startF.getValue());
		reader.setEnd(endF.getValue());
	}

	public void setReaderReadData() {
		reader.setReadData(readData);
	}

	public void setSortOption() {
		String q = "&SortType=";
		if (sortDefault.isSelected()) {
			reader.setSortOption(q + "default");
		} else if (sortBought.isSelected()) {
			reader.setSortOption(q + "total_tranpro_desc");
		} else if (sortNew.isSelected()) {
			reader.setSortOption(q + "create_desc");
		} else if (sortPriceAsc.isSelected()) {
			reader.setSortOption(q + "price_asc");
		} else {
			reader.setSortOption(q + "price_desc");
		}
	}

	/*
	 * reader callback method
	 */
	public void setTargetCount(int count) {
		Platform.runLater(() -> {
			setSearchedProducts(count + "");
		});
	}

	public void setSearchedProducts(String data) {
		Platform.runLater(() -> {
			searchedProduct.setText(data);
		});
	}

	public void setLimitPage(String data) {
		Platform.runLater(() -> {
			limitPage.setText(data);
		});
	}

	public void setStartBtnEnable() {
		Platform.runLater(() -> {
			startBtn.setDisable(false);
		});
	}

	public void setImgNum(String text) {
		Platform.runLater(() -> {
			imgF.setText(text);
		});
	}

	public void setTargetPageCnt(int cnt) {
		targetPageCnt = (double) cnt;
	}

	/*
	 * differ method area
	 */
	public void differInitialize() {
		setImgDiffPath();
		setReaderAlive();
		setDifferReadData();
		setDifferPassedData();
		setDifferImgNum();
	}

	public void setImgDiffPath() {
		if (imgPath != null)
			differ.setImageDiffPath(imgPath.getAbsolutePath());
	}

	public void setReaderAlive() {
		differ.setReaderAlive(reader.getAlive());
	}

	public void setDifferReadData() {
		differ.setReadData(readData);
	}

	public void setDifferPassedData() {
		differ.setPassedData(passedData);
	}

	public void setDifferImgNum() {
		differ.setImgNumber(imgNum);
	}

	/*
	 * crawler method area
	 */
	public void crawlerInitialize() {
		setCrawlerPassedData();
		setCrawlerCrawledData();
		setCrawlerDifferAlive();
		setCrawlerDelay();
		setCrawlerLogoSrc();
	}

	public void setCrawlerPassedData() {
		crawler.setPassedData(passedData);
	}

	public void setCrawlerCrawledData() {
		crawler.setCrawledData(crawledData);
	}

	public void setCrawlerDifferAlive() {
		crawler.setDifferAlive(differ.getAlive());
	}

	public void setCrawlerDelay() {
		crawler.setDelay(crawlDelayF.getValue() * 1000l);
	}

	public void setCrawlerLogoSrc() {
		crawler.setLogo(logoSrc.getText());
	}

	/*
	 * writer method area
	 */
	public void writerInitialize() {
		setWriterAS();
		setWriterCid();
		setWriterConversionValue();
		setWriterCrawledData();
		setWriterCrawlerAliver();
		setWriterHF();
		setWriterUSPN();
	}

	public void setWriterCrawledData() {
		writer.setCrawledData(crawledData);
	}

	public void setWriterConversionValue() {
		writer.setConversionValue(moneyF.getValue() == 0 ? 1200 : moneyF.getValue());
	}

	public void setWriterCrawlerAliver() {
		writer.setCrawlerAlive(crawler.alive());
	}

	public void setWriterAS() {
		writer.setAfterServiceContent(a_sA.getText());
		writer.setAfterServicePhoneNumber(a_sF.getText());
	}

	public void setWriterHF() {
		writer.setHeader(headerA.getText());
		writer.setFooter(footerA.getText());
	}

	public void setWriterCid() {
		writer.setCategoryID(categoryTF.getText());
	}

	public void setWriterUSPN() {
		writer.setUserProductName(userPNTF.getText());
	}

	/*
	 * etc method area
	 */
	public void toggleNaver() {
		Writer.toggleNaver();
	}

	public void toggleSmelch() {
		Writer.toggleSmelch();
	}

	public String readFile(File f) {
		try {
			return FileUtils.readFileToString(f, "UTF-8");
		} catch (IOException e) {
			return "";
		}
	}

	public void saveFile(File f, String data) {
		try {
			FileUtils.write(f, data, "UTF-8", false);
		} catch (IOException e) {
		}
	}

	public void setImgPath() {
		DirectoryChooser chooser = new DirectoryChooser();
		chooser.setTitle("이미지 중복검사를 위한 폴더를 선택해주세요.");
		imgPath = chooser.showDialog(null);
		if (imgPath != null) {
			System.out.println(imgPath.getAbsolutePath());
			imgPathStatus.setText("O");
			imgPathStatus.setTextFill(Color.web("#9048ff"));
		}
	}

	public void setProgress() {
		Thread progressUpdater = new Thread() {
			private boolean flag = true;

			public void run() {
				while (flag) {
					Platform.runLater(() -> {
						crawledLinkCnt.setText((int) reader.getReadCount() + "");
						targetLinkCnt.setText((int) targetPageCnt + "");
						passedDataCnt.setText((int) differ.getPassedCount() + "");
						crawledDataCnt_1.setText((int) crawler.getCrawledCount() + "");
						crawledDataCnt_2.setText((int) crawler.getCrawledCount() + "");
						wroteDataCnt.setText((int) writer.getWroteCount() + "");
						readProgress.setProgress(reader.getReadCount() / targetPageCnt);
						crawlProgress.setProgress(crawler.getCrawledCount() / differ.getPassedCount());
						writeProgress.setProgress(writer.getWroteCount() / crawler.getCrawledCount());
						leftCount.setText(readData.size() + "");
					});
					delay();
					if (!writer.isWriterAlive()) {
						flag = false;
						changeExcelName();
						Platform.runLater(() -> {
							restartAlert();
						});
					}
				}

			}

			public void delay() {
				try {
					Thread.sleep(1111L);
				} catch (InterruptedException e) {
				}
			}
		};
		progressUpdater.start();
	}

	public void restartAlert() {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("수집 완료");
		alert.setHeaderText("수집이 완료되었습니다.");
		alert.setContentText("검색 목표 상품 갯수" + targetLinkCnt.getText() + "\r\n" + "중복되지 않은 상품 갯수"
				+ passedDataCnt.getText() + "\r\n" + "수집된 상품 갯수" + crawledDataCnt_1.getText() + "\r\n" + "엑셀 작성 완료 갯수"
				+ wroteDataCnt.getText() + "\r\n");
		alert.showAndWait().ifPresent(rs -> {
			if (rs == ButtonType.OK) {
				restartBtn.setDisable(false);
				alert.close();
			}
		});
	}

	public void cannotStartAlert() {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("수집 불가");
		alert.setHeaderText("수집이 불가능합니다.");
		alert.setContentText("naver 혹은 smelch 둘 중 하나라도 선택되어야 합니다.");
		alert.showAndWait().ifPresent(rs -> {
			if (rs == ButtonType.OK) {
				alert.close();
			}
		});
	}

	public void restart() {
		reader.exit();
		saveConfs();
		Main.restart();
	}

	public void deleteAllOutput() {
		try {
			for (int i = 0; i < 10; i++)
				FileUtils.deleteDirectory(new File("output/" + i));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void changeExcelName() {
		int totalWroteCnt = (int) Writer.getInstance().getWroteCount();
		int numberOfDir = totalWroteCnt / 500;
		int last = Boundaries.size();
		String[] boundaries = new String[last--];
		boundaries = Boundaries.toArray(boundaries);
		if (Writer.useSmelch()) {
			String srcPath = "output/0/smelch.xls";
			String destName = getRenameString(boundaries[0], boundaries[last]) + " 멸치.xls";
			renameAndCopyExcel(srcPath, destName);
		}
		if (Writer.useNaver()) {
			for (int i = 0; i <= numberOfDir; i++) {
				String path = "output/" + i;
				String srcPath = path + "/naver.xls";
				String destName = getRenameString(boundaries[i*2], boundaries[i*2 + 1]) + " 네이버.xls";
				renameAndCopyExcel(srcPath, destName);
			}
		}
	}

	public String getRenameString(String start, String end) {
		return start + "~" + end + " - " + userPNTF.getText();
	}

	public void renameAndCopyExcel(String srcPath, String destName) {
		Path movedPath = renameFile(srcPath, destName);	// rename 부분
		Path copyPath = FileSystems.getDefault().getPath(imgPath.getPath() + "\\" +destName);
		try {
			Files.copy(movedPath, copyPath, StandardCopyOption.REPLACE_EXISTING); // copy 부분
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public Path renameFile(String oldPath, String newName) {
		Path movedPath = null;
		try {
			Path source = Paths.get(oldPath);
			movedPath = Files.move(source, source.resolveSibling(newName), StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return movedPath;
	}
}

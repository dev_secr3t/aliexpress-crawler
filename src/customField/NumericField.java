package customField;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.Initializable;
import javafx.scene.control.TextField;

public class NumericField extends TextField implements Initializable{

	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
	}
	
	@Override
	public void replaceText(int start, int end, String text) {
		if(text.matches("[0-9]*")) {
			super.replaceText(start, end, text);
		}
	}
	
	@Override
	public void replaceSelection(String replacement) {
		if(replacement.matches("[0-9]*")) {
			super.replaceSelection(replacement);
		}
	}
	
	public int getValue() {
		try{
			return Integer.parseInt(getText());
		} catch (NumberFormatException nfe) {
			return 0;
		}
	}
}

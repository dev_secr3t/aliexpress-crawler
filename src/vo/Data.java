package vo;

import static utils.MyObjects.nonNullOrDefault;

public class Data {

    public String productId;
    public int dir;
    private String name;
    private double price;
    private String mainImg;
    private String details;
    private String optionNames = "";
    private String optionValues = "";
    private String keyword;
    private String picUrl;
    private String detailUrl;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    public String getDetailUrl() {
        return detailUrl;
    }

    public void setDetailUrl(String detailUrl) {
        this.detailUrl = detailUrl;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name.replaceAll("[*]", "x");
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getMainImg() {
        return mainImg;
    }

    public void setMainImg(String mainImg) {
        this.mainImg = mainImg;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getOptionNames() {
        return optionNames;
    }

    public void setOptionNames(String optionNames) {
        this.optionNames = nonNullOrDefault(optionNames, "");
    }

    public String getOptionValues() {
        return optionValues;
    }

    public void setOptionValues(String optionValues) {
        this.optionValues = nonNullOrDefault(optionValues, "");
    }

    @Override
    public String toString() {
        return "Data [name=" + name + ", price=" + price + ", mainImg=" + mainImg + ", details=" + details
                + ", optionNames=" + optionNames + ", optionValues=" + optionValues + ", keyword=" + keyword
                + ", picUrl=" + picUrl + ", detailUrl=" + detailUrl + "]";
    }
}
